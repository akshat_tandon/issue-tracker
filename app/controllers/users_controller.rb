class UsersController < ApplicationController
  before_action :set_user, only: [:show, :update, :destroy, :show_projects]

  # GET /users
  # GET /users.json
  def index
    @users = User.all

    render json: @users
  end

  # GET /users/1
  # GET /users/1.json
  def show
    render json: @user
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    if @user.save
      render json: @user, status: :created, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    @user = User.find(params[:id])

    if @user.update(user_params)
      head :no_content
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy

    head :no_content
  end

  # Custom action for sending all the projects associated with a user
  def show_projects
    project_list = @user.projects
    render json: project_list
  end

  #Custom action for checking password,return the user tuple if password macthes ,error otherwise
  def check_password
    temp_user = User.where(email: params[:email]).first
    puts "----------------------"
    puts temp_user
    puts params[:password]
    puts temp_user[:password]
    puts "----------------------"
    if temp_user != nil
      if params[:password] == temp_user[:password]
        render json: temp_user
      else
        render json: temp_user, status: :unprocessable_entity
      end
    else
      render json: temp_user, status: :unprocessable_entity
    end
  end





  private

    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(:name, :email, :password)
    end
end
