'use strict';

/**
 * @ngdoc overview
 * @name issueTrackerApp
 * @description
 * # issueTrackerApp
 *
 * Main module of the application.
 */
angular
  .module('issueTrackerApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/signup', {
        templateUrl: 'views/signup.html',
        controller: 'SignupCtrl'
      })
      .when('/home', {
        templateUrl: 'views/home.html',
        controller: 'HomeCtrl'
      })
      .when('/create_project', {
        templateUrl: 'views/create_project.html',
        controller: 'CreateProjectCtrl'
      })
      .when('/show_projects', {
        templateUrl: 'views/show_projects.html',
        controller: 'ShowProjectsCtrl'
      })
      .when('/project_page', {
        templateUrl: 'views/project_page.html',
        controller: 'ProjectPageCtrl'
      })
      .when('/edit_project', {
        templateUrl: 'views/edit_project.html',
        controller: 'EditProjectCtrl'
      })
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
