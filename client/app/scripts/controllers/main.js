'use strict';

/**
 * @ngdoc function
 * @name issueTrackerApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the issueTrackerApp
 */
angular.module('issueTrackerApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
