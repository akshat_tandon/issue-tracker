'use strict';

angular.module('issueTrackerApp')
.controller('ShowProjectsCtrl', [ '$http', '$rootScope', '$window','$scope', '$location', function ($http, $scope, $rootScope, $window, $location) {
  
  $scope.project_list;
  $scope.status_message ='Hi';
  $rootScope.project_id = "";
  
  var result = $http.post('/api/show_projects',{'id':$rootScope.user.id});
  
  result.success(function(obj){
    $scope.status_message = 'Projects list retrieved successfully';
    $scope.project_list = obj;
  });

  result.error(function(obj){
    $scope.status_message = 'Error in retrieving projects';
  });

  $scope.explore = function(tempId){
    $rootScope.project_id = tempId;
    $location.path("/project_page");
    
  }



}]);

