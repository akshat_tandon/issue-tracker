'use strict';

/**
 * @ngdoc function
 * @name issueTrackerApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the issueTrackerApp
 */
angular.module('issueTrackerApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
