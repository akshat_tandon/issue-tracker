'use strict';

angular.module('issueTrackerApp')
.controller('CreateProjectCtrl', [ '$http', '$rootScope', '$window','$scope', function ($http, $scope, $rootScope, $window) {
  $scope.project = {};	
  $scope.project.title = '';
  $scope.project.description = '';
  $scope.greeting = 'Hello';
  $scope.current_project
  $scope.submit = function() {
    $scope.greeting = 'Entered';
    
    var result = $http.post('/api/projects.json',{ 'project':{'title':$scope.project.title,
                            'description':$scope.project.description, 'owner':$rootScope.user.email, 'is_public':true } });
    result.success(function(obj){
      $scope.greeting = "Results sent to server";
      $scope.current_project = obj
      $scope.greeting = 'Task by name ' + obj.title + ' created';

      
      $scope.greeting = 'Sending owner details';
      var result = $http.post('/api/project_add_user',{'id_project':$scope.current_project.id,'id_user':$rootScope.user.id});

      result.success(function(obj){
        $scope.greeting = ' Owner added to project list '+obj.title;
      });
              
      result.error(function(obj){
       $scope.greeting = 'Owner not added,ERROR';
       });
      
    });
    result.error(function(obj){
      $scope.greeting = "Error";
    });


  };
}]);

