'use strict';

angular.module('issueTrackerApp')
.controller('LoginCtrl', [ '$http', '$rootScope', '$window','$scope', function ($http, $scope, $rootScope, $window) {
  $scope.user = {};	
  
  $scope.user.email = "";
  $scope.user.password = "";
  $scope.greeting = "Hello";
  $scope.submit = function() {
  	$scope.greeting = "Entered";
    var result = $http.post('/api/check_login_password',{"email":$scope.user.email,"password":$scope.user.password});
    result.success(function(obj){
    	$scope.greeting = "Results sent to server";
    	$rootScope.user = obj;
    	$scope.greeting = $rootScope.user.name;
     
    });
    result.error(function(obj){
    	$scope.greeting = "Error,not able to login ";
    });

  };
}]);


