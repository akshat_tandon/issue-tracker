'use strict';

angular.module('issueTrackerApp')
.controller('ProjectPageCtrl', [ '$http', '$rootScope', '$window','$scope', '$location', function ($http, $scope, $rootScope, $window, $location) {
  $scope.status_message = "Project id " + $rootScope.project_id;
  $scope.latest_project = null;

  var result = $http.get('/api/projects/' + $rootScope.project_id);
 
  result.success(function(obj){
    $scope.status_message = 'Project successfully retrieved from ' + '/api/projects/' + obj.id;
    $scope.latest_project = obj;
    $rootScope.project_id = null;
  });

  result.error(function(obj){
    $scope.status_message = 'Error in retrieving projects';
    $rootScope.project_id = null;
  });

  $scope.edit_project_function = function(){
    $rootScope.edit_project_id = $scope.latest_project.id;
    $location.path("/edit_project");
  };
  
  

}]);