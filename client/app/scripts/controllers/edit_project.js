'use strict';

angular.module('issueTrackerApp')
.controller('EditProjectCtrl', [ '$http', '$rootScope', '$window','$scope', function ($http, $scope, $rootScope, $window) {
  
  $scope.project = {};	
  $scope.project.title = '';
  $scope.project.description = '';
  $scope.greeting = 'Hello';
  
  $scope.submit = function() {
    $scope.greeting = 'Entered';
    
    var result = $http.put('/api/projects/' + $rootScope.edit_project_id,{ 'project':{'title':$scope.project.title,
                            'description':$scope.project.description, 'owner':$rootScope.user.email, 'is_public':true } });
    result.success(function(obj){
      $scope.greeting = "Results sent to server";
      $scope.greeting = 'Project edited';
      
    });
    result.error(function(obj){
      $scope.greeting = "Error";
    });


  };
}]);

